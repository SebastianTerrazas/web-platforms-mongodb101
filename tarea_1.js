/*
    S = Sentence
    Q = Question
    C = Command
    A = Answer
*/

/* 1)
    S:  Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
    C:  mongoimport -d students -c grades < grades.json  
*/

/* 2) 
    S:  El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los 
        siguientes comandos en la terminal de mongo: >use students; >db.grades.count() 
    Q:  ¿Cuántos registros arrojo el comando count?
    C:  > use students
        > db.grades.count()
    A:  800
*/
/* 3) 
    Q:  Encuentra todas las calificaciones del estudiante con el id numero 4.
    C:  > db.grades.find( { student_id : 4} ).pretty()
    A:  exam : 87.89071881934647
        quiz :  27.29006335059361
        homework : 28.656451042441
        homework : 5.244452510818443
*/

/* 4) 
    Q:  ¿Cuántos registros hay de tipo exam?
    C:  > db.grades.find( { type : "exam" } ).count()
    A:  200
*/

/* 5) 
    Q:  ¿Cuántos registros hay de tipo homework?
    C:  > db.grades.find( { type : "homework" } ).count()
    A:  400
*/

/* 6) 
    Q:  ¿Cuántos registros hay de tipo quiz?
    C:  > db.grades.find( { type : "quiz" } ).count()
    A:  200
*/

/* 7) 
    Q:  Elimina todas las calificaciones del estudiante con el id numero 3
    C:  > db.grades.remove( { student_id : 3 } )
    A:  WriteResult({ "nRemoved" : 4 })
*/

/* 8) 
    Q:  ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
    C:  > db.grades.find( { type : "homework", score : 75.29561445722392 } , { student_id : 1 } )
    A:  El estudiante con el student_id = 9
*/

/* 9) 
    Q:  Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    C:  > db.grades.update( { "_id" : ObjectId("50906d7fa3c412bb040eb591") }, { $set : { score : 100 } } )
    A:  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
*/

/* 10) 
    Q:  ¿A qué estudiante pertenece esta calificación?
    C:  > db.grades.find( { "_id" : ObjectId("50906d7fa3c412bb040eb591") } , { student_id : 1 } )
        or
        > db.grades.find( { score : 100 } , { student_id : 1 } )
    A:  El estudiante con el student_id = 6
*/